<Qucs Schematic 0.0.20>
<Properties>
  <View=336,45,990,735,1.43731,0,0>
  <Grid=10,10,1>
  <DataSet=tx_harmonics_flt.dat>
  <DataDisplay=tx_harmonics_flt.dpl>
  <OpenDisplay=1>
  <Script=tmp.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 390 260 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *1 5 390 290 0 0 0 0>
  <GND *2 5 500 290 0 0 0 0>
  <Eqn Eqn1 1 620 370 -28 15 0 0 "S21_dB=dB(S[2,1])" 1 "S11_dB=dB(S[1,1])" 1 "yes" 0>
  <GND *5 5 790 280 0 0 0 0>
  <C C1 1 500 260 17 -26 0 1 "5.6 pF" 1 "" 0 "neutral" 0>
  <C C4 1 570 140 -26 -55 0 2 "6.8pF" 1 "" 0 "neutral" 0>
  <L L1 1 570 180 -26 10 0 0 "4.7 nH" 1 "" 0>
  <Pac P2 1 790 250 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <.SP SP1 1 400 360 0 67 0 0 "log" 1 "200 MHz" 1 "1200 MHz" 1 "201" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Pac P4 1 660 620 18 -26 0 1 "4" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <Pac P3 1 420 620 18 -26 0 1 "3" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 5 550 670 0 0 0 0>
  <Eqn Eqn2 1 840 600 -28 15 0 0 "S43_dB=dB(S[4,3])" 1 "S33_dB=dB(S[3,3])" 1 "yes" 0>
  <SPfile X1 1 550 590 -26 -59 0 0 "LFCN-490+_Unit1.s2p" 1 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
</Components>
<Wires>
  <500 180 500 230 "" 0 0 0 "">
  <500 180 540 180 "" 0 0 0 "">
  <600 180 640 180 "" 0 0 0 "">
  <500 140 500 180 "" 0 0 0 "">
  <500 140 540 140 "" 0 0 0 "">
  <600 140 640 140 "" 0 0 0 "">
  <640 140 640 180 "" 0 0 0 "">
  <640 180 790 180 "" 0 0 0 "">
  <790 180 790 220 "" 0 0 0 "">
  <390 180 390 230 "" 0 0 0 "">
  <390 180 500 180 "" 0 0 0 "">
  <420 590 520 590 "" 0 0 0 "">
  <580 590 660 590 "" 0 0 0 "">
  <420 650 420 670 "" 0 0 0 "">
  <420 670 550 670 "" 0 0 0 "">
  <660 650 660 670 "" 0 0 0 "">
  <550 670 660 670 "" 0 0 0 "">
  <550 620 550 670 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
